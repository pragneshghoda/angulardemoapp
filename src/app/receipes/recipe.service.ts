import { Recipe } from "./recipe.model";
import { Injectable } from "@angular/core";
import { Ingredient } from "../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list/shopping-list.service";
import { Subject } from "rxjs/Subject";

@Injectable()
export class RecipeService {

    recipeChanged = new Subject<Recipe[]>();

    private recipies: Recipe[] = [
        new Recipe("Tasty Schnitzel",
            "A super-tasty Schnitzel - just awesome!",
            "https://media-cdn.tripadvisor.com/media/photo-s/0b/d8/d6/fd/schnitzel-tasty-tasty.jpg",
            [
                new Ingredient("Meat", 1),
                new Ingredient("French Fries", 20),
            ]),
        new Recipe("Big Fat Burger ",
            "what else you need to say ?",
            "https://static1.squarespace.com/static/55a985b9e4b0f561249e6e6f/t/5628027be4b0bace7184603c/1445462662408/FAtFacebook.png?format=2500w",
            [
                new Ingredient("Bread", 2),
                new Ingredient("Meat", 1)
            ])
    ];

    constructor(private slService: ShoppingListService) {
    }

    setRecipes(recipes: Recipe[]) {
        this.recipies = recipes;
        this.recipeChanged.next(this.recipies.slice());
    }

    getRecipes() {
        return this.recipies.slice();
    }

    getRecipe(index: number) {
        return this.recipies[index];
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
    }

    addRecipe(recipe: Recipe) {
        this.recipies.push(recipe);
        this.recipeChanged.next(this.recipies.slice());
    }

    updateRecipe(index: number, newRecipe: Recipe) {
        this.recipies[index] = newRecipe;
        this.recipeChanged.next(this.recipies.slice());
    }
    deleteRecipe(index: number) {
        this.recipies.splice(index, 1);
        this.recipeChanged.next(this.recipies.slice());
    }
}