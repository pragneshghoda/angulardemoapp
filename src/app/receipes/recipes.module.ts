import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../shared/shared.module";
import { ReceipeDetailComponent } from "./receipe-detail/receipe-detail.component";
import { ReceipeItemComponent } from "./receipe-list/receipe-item/receipe-item.component";
import { ReceipeListComponent } from "./receipe-list/receipe-list.component";
import { RecipeEditComponent } from "./recipe-edit/recipe-edit.component";
import { RecipeStartComponent } from "./recipe-start/recipe-start.component";
import { RecipesRoutingModule } from "./recipes-routing.module";
import { RecipesComponent } from "./recipes.component";

@NgModule({
    declarations: [
        RecipesComponent, RecipeStartComponent, ReceipeDetailComponent,
        ReceipeListComponent, ReceipeItemComponent, RecipeEditComponent
    ],
    imports: [
        CommonModule, ReactiveFormsModule, RecipesRoutingModule, SharedModule
    ],
    providers: []
})
export class RecipesModule {

}