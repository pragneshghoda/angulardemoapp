import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth/auth-guard.service';
import { AuthModule } from './auth/auth.module';
import { AuthService } from './auth/auth.service';
import { BlogComponentComponent } from './blog-component/blog-component.component';
import { HeaderComponent } from './header/header.component';
import { RecipeService } from './receipes/recipe.service';
import { RecipesModule } from './receipes/recipes.module';
import { DataStorageService } from './shared/data-storage.service';
import { SharedModule } from './shared/shared.module';
import { ShoppingListModule } from './shopping-list/shopping-list.module';
import { ShoppingListService } from './shopping-list/shopping-list.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BlogComponentComponent
  ],
  imports: [
    BrowserModule, HttpModule, AppRoutingModule, RecipesModule, SharedModule, ShoppingListModule, AuthModule  
  ],
  providers: [ShoppingListService, RecipeService, DataStorageService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
